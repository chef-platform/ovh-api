# frozen_string_literal: true

name 'ovh-api'
maintainer 'Chef Platform'
maintainer_email 'incoming+chef-platform/ovh-api@incoming.gitlab.com'
license 'Apache-2.0'
description 'Configure and control your servers on OVH by its API'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url 'https://gitlab.com/chef-platform/ovh-api'
issues_url 'https://gitlab.com/chef-platform/ovh-api/issues'
version '2.5.0'

chef_version '>= 12.14'

supports 'centos'
supports 'debian'
supports 'ubuntu'

depends 'ohai', '>= 4.0'
