# frozen_string_literal: true

#
# Copyright (c) 2016-2017 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'
require 'socket'

ip = Socket.getifaddrs.find do |ifaddr|
  ifaddr.name == 'eth0' && ifaddr.addr.ip? && ifaddr.addr.ipv4?
end.addr.ip_address

result = {
  'hardware' =>
  {
    'coresPerProcessor' => 10,
    'defaultHardwareRaidSize' => { 'unit' => 'GB', 'value' => 2880 },
    'defaultHardwareRaidType' => 'raid10',
    'description' => 'Serveur Infrastructure BIG-HG 2U (max 12 disques)',
    'diskGroups' =>
    [{
      'diskSize' => { 'unit' => 'GB', 'value' => 480 },
      'diskType' => 'SSD',
      'numberOfDisks' => 12,
      'raidController' => 'MegaRaid 9271-4i'
    }],
    'memorySize' => { 'unit' => 'MB', 'value' => 262_144 },
    'motherboard' => 'X10DRH-iT',
    'numberOfProcessors' => 2,
    'processorArchitecture' => 'x86_64',
    'processorName' => 'E5-2660v3',
    'usbKeys' => nil
  },
  'primary_iface' => 'eth0',
  'primary_ip' => ip,
  'service_name' => 'default-ovh.kitchen',
  'type' => 'dedicated',
  'vrack_iface' => 'eth1'
}

describe 'Ohai plugin' do
  it 'node[\'ovh\'] should contain information from OVH API' do
    expect(file('/tmp/ohai-ovh')).to contain result.to_s
  end
end
